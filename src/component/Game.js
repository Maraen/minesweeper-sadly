import React from 'react';
import Board from './Board';

export default class Game extends React.Component{
    state = {
        height: 8,
        width: 8,
        mines: 10,
    }; // big boy here can create context to be passes down
    render(){
        const { height, width, mines } = this.state; // we can change this to use hooks, but is it usefull
        return(
            <div className="game">
                <Board height={height} width={width} mines={mines} />
            </div>
        );
    }
}